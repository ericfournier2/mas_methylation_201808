# Generate bismark alignment jobs
for trim in output/trim/*.fastq.gz
do
    sample=`echo $trim | sed -e 's/.*\///' | sed -e 's/.fastq.gz//'`

    mkdir -p jobs
    script=jobs/$sample.chrM.bismark.sh
    cat <<EOF > $script
#!/bin/bash
module load mugqic/bismark
module load samtools
module load bowtie2

mkdir -p output/bismark/chrM/$sample
bismark --non_directional -o output/bismark/chrM/$sample -p 10 input/chrM/ $trim
EOF

    sbatch --time 6:00:00 --mem 32G --cpus-per-task 12 --account def-masirard -D `pwd` -e $script.stderr -o $script.stdout $script
done
