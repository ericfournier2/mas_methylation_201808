#!/bin/bash
#SBATCH --time=8:00:00
#SBATCH --account=def-masirard
#SBATCH --mem=48G
#SBATCH --cpus-per-task=1

module load mugqic/bismark
module load mugqic/samtools
module load mugqic/bowtie2

mkdir -p output/bismark_extractor/chrM
bismark_methylation_extractor --counts --bedGraph --CX_context --cytosine_report --comprehensive --merge_non_CpG -s --genome_folder input/chrM/ -o output/bismark_extractor/chrM output/bismark/chrM/*/*.bam