my $linecount = 0;
my $nocoverage = {"CG"=>0, "CHG"=>0, "CHH"=>0};
my $hascoverage = {"CG"=>0, "CHG"=>0, "CHH"=>0};
my $totalcoverage = {"CG"=>0, "CHG"=>0, "CHH"=>0};
while($line=<STDIN>) {
    chomp($line);
    my @split_line = split("\t", $line);
    my $meth = $split_line[3];
    my $unmeth = $split_line[4];
    
    if($meth + $unmeth == 0) {
        $nocoverage->{$split_line[5]} += 1;
    } else {
        $hascoverage->{$split_line[5]} += 1;
        $totalcoverage->{$split_line[5]} += ($meth + $unmeth);
    }

    ++$linecount;
    #if($linecount % 1000000 == 0) {
    #    print $linecount . "\n";
    #}
}

print "No coverage\t" . $nocoverage->{"CG"} . "\t". $nocoverage->{"CHG"} . "\t". $nocoverage->{"CHH"} . "\n";
print "Has coverage\t" . $hascoverage->{"CG"} . "\t". $hascoverage->{"CHG"} . "\t". $hascoverage->{"CHH"} . "\n";
print "Total coverage\t" . $totalcoverage->{"CG"} . "\t". $totalcoverage->{"CHG"} . "\t". $totalcoverage->{"CHH"} . "\n";
print "Average coverage\t" . $totalcoverage->{"CG"}  / $hascoverage->{"CG"} . "\t". $totalcoverage->{"CHG"} / $hascoverage->{"CHG"} . "\t". $totalcoverage->{"CHH"} / $hascoverage->{"CHH"} . "\n";

