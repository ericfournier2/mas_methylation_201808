#PBS -N gather_coverage
#PBS -A bkk-363-aa
#PBS -l walltime=6:00:00
#PBS -l nodes=1:ppn=16

for report in output/bismark_extractor/chrM/*.CX_report.txt
do
    #perl scripts/coverage.pl < $report > $report.coverage &
    echo $report
done
wait
