# Gather stats
rm trimstats.txt
for trim in jobs/trim.*.sh.stderr
do 
    lib=`echo $trim | sed -e 's/.*trim\.\(.*\).sh.stderr/\1/'`
    input=`grep -F "Input Reads" $trim | sed -e 's/Input Reads: \([0-9\]*\).*/\1/'`
    trimmed=`grep -F "Input Reads" $trim | sed -e 's/.*Surviving: \([0-9\]*\).*/\1/'`
    
    echo $lib","$input","$trimmed >> trimstats.txt
done

rm bismarkstats.txt
for report in  output/bismark/chrM/*/*report.txt
do 
    lib=`echo $report | sed -e 's/.*\/\(.*\)_bismark_bt2_SE_report.txt/\1/'`
    uniquehit=`grep -F "unique best hit" $report | sed -e 's/.*different alignments:[ ]*\([0-9\]*\)/\1/'`
    nohit=`grep -F "any condition" $report | sed -e 's/.*any condition:[ ]*\([0-9\]*\)/\1/'`
    multiplehit=`grep -F "did not map uniquely" $report | sed -e 's/.*did not map uniquely:[ ]*\([0-9\]*\)/\1/'`
    
    echo $lib","$uniquehit","$nohit","$multiplehit >> bismarkstats.txt
done
