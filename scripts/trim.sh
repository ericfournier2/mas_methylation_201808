# Generate trim jobs
for i in raw/*.fastq.gz
do
    sample=`echo $i | sed -e 's/.*Index_[0-9]*.//' | sed -e 's/_R1.fastq.gz//'`
    script=jobs/trim.$sample.sh
    mkdir -p jobs
    cat <<EOF > $script
#!/bin/bash
module load trimmomatic && \
mkdir -p output/trim/$sample && \
java -XX:ParallelGCThreads=1 -Xmx20G -jar $EBROOTTRIMMOMATIC/trimmomatic-0.36.jar SE \
  -threads 6 \
  -phred33 \
  $i \
  output/trim/$sample.fastq.gz \
  ILLUMINACLIP:input/adapters.fa:2:30:15 \
  TRAILING:30 HEADCROP:4 \
  MINLEN:25
EOF
    sbatch --time 2:00:00 --mem 20G --cpus-per-task 6 --account def-masirard -D `pwd` -e $script.stderr -o $script.stdout $script
done
